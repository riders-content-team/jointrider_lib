import rospy
import math
import time
from std_msgs.msg import Float64
from std_srvs.srv import Trigger, Empty
from gazebo_msgs.srv import GetLinkState, GetJointProperties

class Robot:
    def __init__(self, robot_name):
        rospy.init_node("jointrider_controller")

        self.joint_position_publisher = [
            rospy.Publisher("/jointrider/joint_1/set_pose", Float64, queue_size=10),
            rospy.Publisher("/jointrider/joint_2/set_pose", Float64, queue_size=10),
            rospy.Publisher("/jointrider/joint_3/set_pose", Float64, queue_size=10),
            rospy.Publisher("/jointrider/joint_4/set_pose", Float64, queue_size=10)
        ]

        rospy.sleep(0.5)

        self._init_game_controller()
        self._init_services()

        self.num_joints = 4
        self.my_angles = [0] * self.num_joints

    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)

            resp = self.robot_handle()
            self.status = resp.message

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "Free Roam"

            rospy.logerr("Service call failed: %s" % (e,))

    def _check_game_controller(self):
        resp = self.robot_handle()
        self.status = resp.message

    def _init_services(self):
        try:
            rospy.wait_for_service("/gazebo/get_link_state", 1.0)
            rospy.wait_for_service("/gazebo/get_joint_properties", 1.0)
            rospy.wait_for_service("/gazebo/pause_physics", 1.0)
            rospy.wait_for_service("/robot_collided", 1.0)

            self.get_link_state = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
            self.get_joint_properties = rospy.ServiceProxy('/gazebo/get_joint_properties', GetJointProperties)
            self.pause_sim = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
            self.robot_collided = rospy.ServiceProxy('/robot_collided', Trigger)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


    def move_joint(self, joint_number, angle):

#        self.my_angles[joint_number-1] = angle
#
#        t = self.my_angles[0] # base joint
#        xr = math.cos(t)
#        yr = math.sin(t)
#
#        # calculate tip position
#        a = math.pi / 2.0 # pointing straight up
#        x = 0
#        y = 0
#        print 'Joint 1', x * xr, x * yr, y # add base rotation to print
#        y = 1 # start at end of first segment
#        
#        bFail = False
#        for i in range(1, self.num_joints):
#            print 'Joint', i + 1, x * xr, x * yr, y
#            a += self.my_angles[i]
#            x += math.cos(a)
#            y += math.sin(a)
#            if y < -0.05: # give a small tolerance for error
#                bFail = True
#
#        print 'Tip position', x * xr, x * yr, y # add base rotation to print
#
#        if bFail:
#            print 'Cannot move robot into ground'

        if not self.status == "No Robot":
            if not self.status == "Free Roam":
                self._check_game_controller()

            if self.status == "Stop":
                return

            try:
                service_name = "/jointrider/joint_" + str(joint_number) + "/is_set"
                rospy.wait_for_service(service_name, 1.0)

                position_arrived_service = rospy.ServiceProxy(service_name, Trigger)

            except (rospy.ServiceException, rospy.ROSException), e:
                rospy.logerr("Service call failed: %s" % (e,))
                return

            self.joint_position_publisher[joint_number-1].publish(Float64(angle))

            rospy.sleep(0.1)

            resp = position_arrived_service()
            is_position_arrived = resp.success

            while (not is_position_arrived):
                resp = self.get_link_state("link_4", "")
                link_4_position = resp.link_state.pose.position

                resp = self.get_link_state("end_point", "")
                robot_tip_position = resp.link_state.pose.position

                if (robot_tip_position.z < -0.1 or link_4_position.z < -0.1):
                    resp = self.get_joint_properties("joint_" + str(joint_number))
                    self.joint_position_publisher[joint_number-1].publish(Float64(resp.position[0]))    
                    self.robot_collided()

                    break

                self.joint_position_publisher[joint_number-1].publish(Float64(angle))
                resp = position_arrived_service()
                is_position_arrived = resp.success



    def is_ok(self):
        if not rospy.is_shutdown():
            rospy.sleep(0.05)
            return True
        else:
            return False
